/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.biggerstacks.mixin.compat.ae2;

import appeng.client.gui.AEBaseScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import portb.biggerstacks.util.SlotLimitHelper;

@Mixin(AEBaseScreen.class)
public class AEBaseScreenMixin
{
    @ModifyConstant(method = "checkHotbarKeys", constant = @Constant(intValue = 64), remap = false)
    private int increaseStackLimit(int val)
    {
        return SlotLimitHelper.getNewStackSize();
    }
}
